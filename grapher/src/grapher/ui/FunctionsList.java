package grapher.ui;

import java.awt.BorderLayout;
import java.awt.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import java.util.*;

public class FunctionsList extends JPanel implements ActionListener ,MouseListener ,ListSelectionListener{
	JList<String> list;
	DefaultListModel<String> listmodel;
	JToolBar buttons ;

	Grapher g;

	public FunctionsList(String[] expressions, Grapher g) {
		this.g = g;
		listmodel = new DefaultListModel<String>();
		for (int i = 0; i < expressions.length; i++) {
			this.addFunction(expressions[i]);
		}
		list = new JList<>(listmodel);
		list.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		
		list.addMouseListener(this); 
		list.addListSelectionListener(this);
			
		this.setLayout(new BorderLayout());
		this.add(list,BorderLayout.CENTER);
	    buttons = new JToolBar ();
		JButton plus = new JButton(" + "); 
		JButton moins = new JButton(" - ");
		plus.addActionListener(this);
		moins.addActionListener(this);
		
		buttons.add(plus);
		buttons.add(moins);
		this.add(buttons,BorderLayout.SOUTH);
		
		
		

	}

	public void addFunction(String name) {

		listmodel.addElement(name);
	}

	public JList<String> getJList() {
		return this.list;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if (e.getSource()==this.buttons.getComponentAtIndex(0)) {
			
			
			DialogBox b =new DialogBox();
			try {
			this.g.add(b.getFunction());
			listmodel.addElement(b.getFunction());
			} 
			catch (RuntimeException ex) {
				
			}
			
			
			
		}
		else {
			if (e.getSource()==this.buttons.getComponentAtIndex(1)) {
				ArrayList<String> r = this.getSelectedItems();
				for (Iterator<String> i=r.iterator();i.hasNext();) {
					String s = i.next();
					listmodel.removeElement(s);
					this.g.removeFunction(s);
					
				}
				
				
			}
		}
		
	}
	public ArrayList<String> getSelectedItems () {
		ArrayList<String> layers = new ArrayList<String>();
		  for (Object selected : list.getSelectedValues()) {
		    layers.add((String) selected);
		  }
		  return layers;
		
		
		
		
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		
		
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void valueChanged(ListSelectionEvent e) {
		ArrayList<String> selected = getSelectedItems();
		this.g.resetSelectedFunction();
		this.g.setSelectedFunction(selected);
		
	}



}