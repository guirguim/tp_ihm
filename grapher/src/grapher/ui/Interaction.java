package grapher.ui;

import java.awt.Cursor;
import java.lang.Math;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

public class Interaction implements MouseListener, MouseMotionListener, MouseWheelListener {

	Grapher g;
	Point p;

	enum State{
		Initial,Left_Pressed,Left_Dragged,Right_Pressed,Right_Dragged
	}

	State current_State;

	public Interaction(Grapher grapher) {
		g = grapher;
		current_State = State.Initial;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
	}

	@Override
	public void mousePressed(MouseEvent e) {

		switch(current_State){

			case Initial:
				if (e.getButton() == MouseEvent.BUTTON1) {
					p = e.getPoint();
					current_State = State.Left_Pressed;
				}
				if (e.getButton() == MouseEvent.BUTTON3) {
					p = e.getPoint();
					g.p1_Rect = p;
					current_State = State.Right_Pressed;
				}
				break;

			default:
				break;
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {

		switch(current_State){

			case Left_Dragged:
				Cursor cursor = new Cursor(Cursor.DEFAULT_CURSOR);
				g.setCursor(cursor);
				p = e.getPoint();
				current_State = State.Initial;
				break;

			case Left_Pressed:
				g.zoom(p, 5);
				p = e.getPoint();
				current_State = State.Initial;
				break;

			case Right_Dragged:
				g.draw_Rect = false;
				g.zoom(g.p1_Rect, g.p2_Rect);
				p = e.getPoint();
				current_State = State.Initial;
				break;

			case Right_Pressed:
				g.zoom(p, -5);
				p = e.getPoint();
				current_State = State.Initial;
				break;

			default:
				break;
		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		p = e.getPoint();
	}

	@Override
	public void mouseExited(MouseEvent e) {
		p = e.getPoint();
	}

	@Override
	public void mouseDragged(MouseEvent e) {

		switch(current_State){

			case Left_Pressed:
				if (p.distance(e.getPoint()) > 5) {
					Cursor cursor = new Cursor(Cursor.HAND_CURSOR);
					g.setCursor(cursor);
					g.translate((int) ( e.getX()-p.getX() ), (int) ( e.getY()-p.getY() ));
					p = e.getPoint();
					current_State = State.Left_Dragged;
				}
				break;

			case Left_Dragged:
				g.translate((int) ( e.getX()-p.getX() ), (int) ( e.getY()-p.getY() ));
				p = e.getPoint();
				current_State = State.Left_Dragged;
				break;

			case Right_Pressed:
				if (p.distance(e.getPoint()) > 5) {
					g.draw_Rect = true;
					g.p2_Rect = e.getPoint();
					g.repaint();
					p = e.getPoint();
					current_State = State.Right_Dragged;
				}
				break;

			case Right_Dragged:
				g.draw_Rect = true;
				g.p2_Rect = e.getPoint();
				g.repaint();
				p = e.getPoint();
				current_State = State.Right_Dragged;
				break;

			default:
				break;
		}
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		p = e.getPoint();
	}

	public void mouseWheelMoved(MouseWheelEvent e) {

		switch(current_State){

			case Initial:
				int n = e.getWheelRotation();
				if (n < 0) {
					g.zoom(p, 5);
					current_State = State.Initial;
				} else {
					g.zoom(p, -5);
					current_State = State.Initial;
				}
				break;

			default:
				break;
		}
	}

}
